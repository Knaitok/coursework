package ru.book.book.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }
    @GetMapping("/authorization")
    public String authorization(Model model) {
        return "authorization";    }
    @GetMapping("/basket")
    public String basket(Model model) {
        return "basket";
    }
    @GetMapping("/addProduct")
    public String addProduct(Model model) {
        return "addProduct";
    }
    @GetMapping("/Editproduct")
    public String Editproduct(Model model) {
        return "Editproduct";
    }
}
